import re
import getpass
import requests


BASE_URL = "https://sharelatex.irisa.fr"
LOGIN_URL = "{}/login".format(BASE_URL)

print("email: ")
email = input()
password = getpass.getpass()
project_id = "5cdd49d92ef2085508de8f02"

zip_url = "{base}/project/{pid}/download/zip".format(base=BASE_URL,
                                                     pid=project_id)

client = requests.session()

# Retrieve the CSRF token first
r = client.get(LOGIN_URL, verify=False)
csrftoken = re.search('(?<=csrfToken = ").{36}', r.text).group(0)

# login
login_data = {"email": email,
              "password":password,
              "_csrf":csrftoken}

r = client.post(LOGIN_URL, data=login_data, verify=False)
r = client.get(zip_url, stream=True)

with open("{}.zip".format(project_id), 'wb') as f:
    for chunk in r.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)

