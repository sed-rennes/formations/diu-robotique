set -xv
# configure raspberry PI has hotspot WiFi, see https://frillip.com/using-your-raspberry-pi-3-as-a-wifi-access-point-with-hostapd/
# mixed with : https://github.com/raspberrypi/documentation/blob/master/configuration/wireless/access-point.aptitude
#
#
# The network passphrase is hardcoded below, please change it
#
#
apt-get -y -install hostapd md dnsmasq

echo "
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
#denyinterfaces wlan0
"  >> /etc/dhcpcd.conf

service dhcpcd restart


echo "# This is the name of the WiFi interface we configured above
interface=wlan0

# Use the nl80211 driver with the brcmfmac driver
driver=nl80211

# This is the name of the network
ssid=Pi3-AP

# Use the 2.4GHz band
hw_mode=g

# Use channel 6
channel=6

# Enable 802.11n
ieee80211n=1

# Enable WMM
wmm_enabled=1

# Enable 40MHz channels with 20ns guard interval
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

# Accept all MAC addresses
macaddr_acl=0

# Use WPA authentication
auth_algs=1

# Require clients to know the network name
ignore_broadcast_ssid=0

# Use WPA2
wpa=2

# Use a pre-shared key
wpa_key_mgmt=WPA-PSK

# The network passphrase
wpa_passphrase=raspberry

# Use AES, instead of TKIP
rsn_pairwise=CCMP" > /etc/hostapd/hostapd.conf

echo '
DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> /etc/default/hostapd

if [ ! -f  /etc/dnsmasq.conf.orig ]; then
    mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
fi

echo "
interface=wlan0      # Use interface wlan0  
listen-address=192.168.4.1 # Explicitly specify the address to listen on  
bind-interfaces      # Bind to the interface to make sure we aren't sending things elsewhere  
server=8.8.8.8       # Forward DNS requests to Google DNS  
domain-needed        # Don't forward short names
bogus-priv           # Never forward addresses in the non-routed address spaces.
dhcp-range=192.168.4.2,192.168.4.122,255.255.255.0,24h # Assign IP addresses between 192.168.4.2 and 192.168.4.122 with a 24 hour lease time 
" > /etc/dnsmasq.conf

echo "
net.ipv4.ip_forward=1" >> /etc/sysctl.conf
echo "1" > /proc/sys/net/ipv4/ip_forward

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
iptables-save > /etc/iptables.ipv4.nat
sed -i 's/^exit 0/iptables-restore < \/etc\/iptables\.ipv4\.nat\nexit 0/g' /etc/rc.local

service hostapd restart
service dnsmasq restart
