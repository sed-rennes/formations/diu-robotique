import re
import getpass
import os
import requests
import zipfile


BASE_URL = "https://sharelatex.irisa.fr"
LOGIN_URL = "{}/login".format(BASE_URL)
print("email: ")
EMAIL = input()
PASSWORD = getpass.getpass()


def download_project(project_id, path='.'):
    zip_url = "{base}/project/{pid}/download/zip".format(base=BASE_URL,
                                                         pid=project_id)

    client = requests.session()

    # Retrieve the CSRF token first
    r = client.get(LOGIN_URL, verify=False)
    csrftoken = re.search('(?<=csrfToken = ").{36}', r.text).group(0)

    # login
    login_data = {"email": EMAIL,
                "password": PASSWORD,
                "_csrf":csrftoken}

    r = client.post(LOGIN_URL, data=login_data, verify=False)
    r = client.get(zip_url, stream=True)

    print("Downloading")
    target_path = os.path.join(path, "{}.zip".format(project_id))
    with open(target_path, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

    print("Unzipping ....")
    zip_file = zipfile.ZipFile(target_path)
    zip_file.extractall(path)
    zip_file.close()


projects = [("cours", "5cb59f883f95c5452a67e7f2"),
            ("tp_light_sensor/", "5cdd49d92ef2085508de8f02"),
            ("tp_follow_lines/", "5ceba19c96e3a0053339bc1c"),
            ("tp_sensors/", "5cf6385e7c107d7dbeadfe00")]

for path, project_id in projects:
    download_project(project_id, path=path)



